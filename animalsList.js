/*
    This routine:
    -- accepts the name of an animal from a user
    -- checks if the animal is in a list of animals
    -- tells the user whether or not the animal is in the list
*/

// animals is a list of animals
let animals = ["fowl", "sheep", "goat", "cattle", "dog", "cat", "fish"];

// animalName holds the name of the animal entered by a user
let animalName = prompt("Guess one of the animals I have in mind: ");

// checks if every character of animalName is an alphabet
if (/^[a-z]+$/i.test(animalName)) {
    if (animals.includes(animalName.toLowerCase())) {
        alert("CORRECT! " + animalName + " is in [" + animals + "]");
    } else {
        alert("WRONG! " + animalName + " not in [" + animals + "]");
    }
} else {
    alert("INVALID! Please enter an all-alphabet input");
}